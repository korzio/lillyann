from django.shortcuts import render, get_object_or_404
from rest_framework import viewsets
from .models import Lesson, UserLesson, LilyannUser
from .serializers import LessonSerializer, UserLessonSerializer, LilyannUserSerializer
from django.contrib.auth.models import User
from api.serializers import UserSerializer
from rest_framework.response import Response


class UserLessonViewset(viewsets.ViewSet):

    def list(self, request):
        user = request.user
        queryset = UserLesson.objects.filter(user__id=user.id)

        return Response(UserLessonSerializer(queryset, many=True, context={'request': request}).data)


class LessonViewset(viewsets.ViewSet):

    def list(self, request):
        user = request.user
        queryset = Lesson.objects.filter(author__id=user.id)

        return Response(LessonSerializer(queryset, many=True, context={'request': request}).data)

    def retrieve(self, request, pk=None):
        queryset = Lesson.objects.all()
        lesson = get_object_or_404(queryset, pk=pk)
        serializer = LessonSerializer(lesson, context={'request': request})
        return Response(serializer.data)



class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer

class LilyannUserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = LilyannUser.objects.all()
    serializer_class = LilyannUserSerializer