from __future__ import unicode_literals

from django.db import models

from django.contrib.auth.models import User


class Lesson(models.Model):
    lang = models.TextField()
    name = models.TextField()
    author = models.ForeignKey('LilyannUser', on_delete=models.CASCADE)
    description = models.TextField()
    url = models.TextField()
    cards = models.ManyToManyField('Card')


class LilyannUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    lessons = models.ManyToManyField(Lesson, through='UserLesson')


class UserLesson(models.Model):
    user = models.ForeignKey(LilyannUser, on_delete=models.CASCADE)
    lesson = models.ForeignKey(Lesson, on_delete=models.CASCADE)
    cards = models.ManyToManyField('LessonCard')


class Translation(models.Model):
    meaning = models.TextField()
    samples = models.TextField()


class Card(models.Model):
    source = models.TextField()
    translations = models.ManyToManyField(Translation)


class LessonCard(models.Model):
    card = models.ForeignKey(Card, on_delete=models.CASCADE)
    accuracy = models.FloatField(default=0.0)
    learned = models.BooleanField(default=False)



