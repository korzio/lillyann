from rest_framework import serializers
from .models import *

class LessonSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Lesson
        fields = ('name', 'author', 'description')

class TranslationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Translation
        fields = ('meaning', 'samples')


class CardSerializer(serializers.ModelSerializer):
    translations = TranslationSerializer(many=True, read_only=True)
    class Meta:
        model = Card
        fields = ('source', 'translations')


class LessonCardSerializer(serializers.ModelSerializer):
    card = CardSerializer(read_only=True)

    class Meta:
        model = LessonCard
        fields = ('card', 'accuracy', 'learned')


class UserLessonSerializer(serializers.ModelSerializer):
    cards = LessonCardSerializer(many=True, read_only=True)
    lesson = serializers.HyperlinkedIdentityField('lesson-detail')

    class Meta:
        model = UserLesson
        fields = ('lesson', 'cards')


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email')

class LilyannUserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = LilyannUser
        fields = ('user', 'lessons')

