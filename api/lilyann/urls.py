"""lilyann URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from rest_framework import routers
import api.views as views
from django.conf.urls import url, include

router = routers.DefaultRouter()
router.register(r'lessons', views.LessonViewset, base_name='lesson')
router.register(r'user_lessons', views.UserLessonViewset, base_name='user_lesson')
router.register(r'authors', views.UserViewSet)
router.register(r'users', views.LilyannUserViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^auth/', include('rest_framework_social_oauth2.urls')),
    url(r'', include('social.apps.django_app.urls')),
    url(r'^admin/', include(admin.site.urls)),
]
