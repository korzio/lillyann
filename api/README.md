# Install

Install [django](http://www.django-rest-framework.org/tutorial/quickstart/)

```
virtualenv env
source env/bin/activate  # On Windows use `env\Scripts\activate`

## Install Django and Django REST framework into the virtualenv
pip install django
pip install djangorestframework
```

# Start

```
## Now sync your database for the first time:
python manage.py migrate

## Start
python manage.py runserver

## GOTO http://127.0.0.1:8000/
```

# Resources

- http://www.django-rest-framework.org/