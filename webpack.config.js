module.exports = [{
  entry: ['./app/client/index.js'],
  output: {
    filename: 'public/client.js',
  },
  devtool: 'source-map',
  module: {
    loaders: [
      {
        test: /.jsx?$/,
        loader: 'babel',
        exclude: /node_modules/,
        query: {
          presets: ['react', 'es2015'],
        },
      },
      {
        test: /\.json$/,
        loader: 'json-loader',
      },
    ],
  },
}, {
  entry: {
    server: './app/server/pages/index.js',
  },
  output: {
    filename: 'public/[name].js',
    libraryTarget: 'umd',
  },
  target: 'node',
  devtool: 'source-map',
  module: {
    loaders: [
      {
        test: /.jsx?$/,
        loader: 'babel',
        exclude: /node_modules/,
        query: {
          presets: ['react', 'es2015'],
        },
      },
      {
        test: /\.json$/,
        loader: 'json-loader',
      },
    ],
  },
}];
