import { expect } from 'chai';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';

import * as lessons from './lessons';
import {
  UPDATE_LESSON,
  ADD_LESSON,
} from './constants';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('lessons actions', () => {
  const store = mockStore({});
  beforeEach(() => {
    nock.cleanAll();
    store.clearActions();
  });

  describe('getLessons()', (done) => {
    it('triggers get request to `lessons` url', () => {
      nock('http://localhost/')
        .get('/api/lessons')
        .reply(200);

      store.dispatch(lessons.getLessons())
        .then(() => done());
    });
  });

  describe('updateLesson()', () => {
    it('triggers UPDATE_LESSON', () => {
      const expectedActions = [
        { type: UPDATE_LESSON, id: 'id', lesson: { name: 'name' } },
      ];

      store.dispatch(lessons.updateLesson(expectedActions[0].id, expectedActions[0].lesson));

      const actions = store.getActions();
      expect(actions).to.deep.equal(expectedActions);
    });
  });

  describe('addNewLesson()', () => {
    it('dispatches ADD_LESSON', () => {
      store.dispatch(lessons.addNewLesson({ name: 'name' }));

      const action = store.getActions().find(({ type }) => type === ADD_LESSON);
      expect(action.type).to.equal(ADD_LESSON);
      expect(action.lesson.name).to.equal('name');
    });
  });

  describe('addLessonRequest()', () => {
    it('triggers put request to `new_lesson` url', done => {
      nock('http://localhost/')
        .put('/api/add_lesson')
        .reply(200);

      store.dispatch(
          lessons.addLessonRequest({ name: 'name' })
        )
        .then(() => done());
    });
  });

  describe('getLesson()', () => {
    it('triggers get request to `lessons/:lessonId` url', done => {
      nock('http://localhost/')
        .get('/api/lessons/test')
        .reply(200);

      store.dispatch(
          lessons.getLesson('test')
        )
        .then(() => done());
    });

    it('triggers ADD_LESSON_WORDS after request', done => {
      nock('http://localhost/')
        .get('/api/lessons/test')
        .reply(200);

      store.dispatch(
          lessons.getLesson('test')
        )
        .then(() => {
          const action = store.getActions()[0];
          expect(action.type).to.equal(ADD_LESSON);

          done();
        });
    });
  });
});
