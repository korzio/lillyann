import axios from 'axios';
// TODO change to custom config file containing only clientID
import { google } from './../../config/social.json';

import {
  LOGIN,
} from './constants';

const onLogIn = (profile) => dispatch => {
  dispatch({
    type: LOGIN,
    profile,
  });
};

const onGoogleLogIn = dispatch => (googleUser) => {
  const profile = googleUser.getBasicProfile();
  const token = googleUser.getAuthResponse().id_token;

  // TODO change api path when it is ready
  axios.post('/api/python/complete/google-plus/', `access_token=${token}`);

  dispatch(onLogIn({
    id: profile.getId(),
    name: profile.getName(),
    image: profile.getImageUrl(),
    email: profile.getEmail(),
  }));
};

// https://developers.google.com/identity/sign-in/web/sign-in
const loginGoogle = dispatch => {
  window.onSignIn = dispatch(onGoogleLogIn);

  const script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = 'https://apis.google.com/js/platform.js';
  script.async = true;
  document.head.appendChild(script);

  const meta = document.createElement('meta');
  meta.name = 'google-signin-client_id';
  meta.content = google.auth.clientID;
  document.head.appendChild(meta);
};

export const loginFlow = () => dispatch => axios.get('/api/auth/')
    .then(response => dispatch(onLogIn(response)))
    .catch(() => loginGoogle(dispatch));
