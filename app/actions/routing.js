import { LOCATION_CHANGE } from './constants';

export const routeInfo = info => dispatch => {
  dispatch({
    type: LOCATION_CHANGE,
    info,
  });
};
