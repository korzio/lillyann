import axios from 'axios';
import {
  UPDATE_WORD,
  ADD_WORD,
  REMOVE_WORD,
} from './constants';

export const updateWord = (id, word) => dispatch => {
  dispatch({
    type: UPDATE_WORD,
    id,
    word,
  });
};

export const addWord = word => ({
  type: ADD_WORD,
  word,
});

export const removeWord = id => ({
  type: REMOVE_WORD,
  id,
});

export const addWordRequest =
  word =>
    dispatch =>
      axios
        .put('/api/add_word', {
          text: word.text,
        })
        .then(response =>
            dispatch(updateWord(word.id, response.data)));

export const translateWordResponse = (id, translateData) =>
  dispatch =>
      dispatch(updateWord(id, {
        translate: translateData.text[0],
      }));

export const translateWord = (id, text) => dispatch => {
  // TODO
  const lang = 'en-nl';
  return axios
      .get('/api/r/translate', {
        params: {
          text,
          lang,
        },
      })
      .then(
        ({ data }) =>
          dispatch(translateWordResponse(id, data))
      );
};
