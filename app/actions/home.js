import axios from 'axios';
import * as wordsActions from './words';
import {
  REMOVE_WORD_HOME,
  SELECT_WORDS_HOME,
  ADD_WORD_HOME,
} from './constants';

export const receiveRecentlyAdded = words => dispatch => {
  words.forEach(word => {
    dispatch({
      type: ADD_WORD_HOME,
      word,
    });

    dispatch(wordsActions.addWord(word));
  });
};

export const addNewWord = text => dispatch => {
  const newWord = Object.assign({
    id: Math.random(),
    text,
  });

  dispatch(receiveRecentlyAdded([newWord]));
  dispatch(wordsActions.addWordRequest(newWord));
};

export const selectWordsHome = selection => ({
  type: SELECT_WORDS_HOME,
  selection,
});

export const getRecentlyAdded = () => dispatch =>
  axios.get('/api/recent')
    .then(({ data: words }) =>
      dispatch(receiveRecentlyAdded(words))
    );

export const isKnownWord = (id, isKnown) => dispatch => {
  dispatch(wordsActions.updateWord(id, { isKnown }));

  dispatch({
    type: REMOVE_WORD_HOME,
    id,
  });

  dispatch({
    type: SELECT_WORDS_HOME,
    selection: [],
  });
};
