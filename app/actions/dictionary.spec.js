import { expect } from 'chai';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';
import { ADD_WORD } from './constants';

import * as dictionary from './dictionary';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('dictionary actions', () => {
  const store = mockStore({});
  beforeEach(() => {
    nock.cleanAll();
    store.clearActions();
  });

  describe('getMyWords()', (done) => {
    it('triggers get request to `dictionary` url', () => {
      nock('http://localhost/')
        .get('/api/dictionary')
        .reply(200);

      store.dispatch(dictionary.getMyWords())
        .then(() => done());
    });
  });

  describe('receiveMyWords()', () => {
    it('dispatches ADD_WORD for each word', () => {
      const items = [1];
      const expectedActions = [
        { type: ADD_WORD, word: 1 },
      ];

      store.dispatch(dictionary.receiveMyWords(items));

      const actions = store.getActions();
      expect(actions).to.deep.equal(expectedActions);
    });
  });
});
