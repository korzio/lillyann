import axios from 'axios';
import {
  UPDATE_LESSON,
  ADD_LESSON,
} from './constants';

export const updateLesson = (id, lesson) => dispatch => {
  dispatch({
    type: UPDATE_LESSON,
    id,
    lesson,
  });
};

export const addLessonRequest = lesson => dispatch => {
  const cleanLesson = Object.assign({}, lesson);
  delete cleanLesson.id;

  return axios
    .put('/api/add_lesson', cleanLesson)
    .then(
    response =>
      dispatch(updateLesson(lesson.id, response.data))
    );
};

export const receiveLessons = lessons => dispatch => {
  lessons.forEach(lesson => {
    dispatch({
      type: ADD_LESSON,
      lesson,
    });
  });
};

export const getLessons = () => dispatch =>
  axios.get('/api/lessons')
    .then(({ data: lessons }) =>
      dispatch(receiveLessons(lessons))
    );

export const getLesson = (lessonId) => dispatch =>
  axios.get(`/api/lessons/${lessonId}`)
    .then(
      ({ data: lesson }) =>
        dispatch(receiveLessons([lesson]))
    );

export const addNewLesson = lesson => dispatch => {
  const newLesson = Object.assign({}, lesson, {
    id: Math.random(),
  });

  dispatch(receiveLessons([newLesson]));
  dispatch(addLessonRequest(newLesson));
};
