import { expect } from 'chai';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';

import * as words from './words';
import {
  UPDATE_WORD,
  ADD_WORD,
  REMOVE_WORD,
} from './constants';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('words actions', () => {
  const store = mockStore({});
  beforeEach(() => {
    nock.cleanAll();
    store.clearActions();
  });

  describe('updateWord()', () => {
    it('triggers UPDATE_WORD', () => {
      store.dispatch(words.updateWord('id', {
        text: 'text',
      }));

      const action = store.getActions()[0];
      expect(action.type).to.equal(UPDATE_WORD);
      expect(action.id).to.equal('id');
      expect(action.word.text).to.equal('text');
    });
  });

  describe('removeWord()', () => {
    it('triggers REMOVE_WORD', () => {
      store.dispatch(words.removeWord('id'));

      const action = store.getActions()[0];
      expect(action.type).to.equal(REMOVE_WORD);
      expect(action.id).to.equal('id');
    });
  });

  describe('addWord()', () => {
    it('dispatches ADD_WORD', () => {
      store.dispatch(words.addWord({ id: 0, text: 'text' }));

      const action = store.getActions()[0];
      expect(action.type).to.equal(ADD_WORD);
      expect(action.word.id).to.equal(0);
      expect(action.word.text).to.equal('text');
    });
  });

  describe('addWordRequest()', () => {
    it('triggers PUT request to `add_word` url', (done) => {
      nock('http://localhost/')
        .put('/api/add_word', {
          text: 'text',
        })
        .reply(200, {
          body: {
            id: '123ABC',
            text: 'text',
          },
        });

      store.dispatch(
        words.addWordRequest({
          text: 'text',
        }))
        .then(() => done());
    });
  });

  describe('translateWord()', (done) => {
    it('triggers GET request to `/api/translate` url', () => {
      nock('http://localhost/')
        .get('/api/translate', {
          text: 'text',
        })
        .reply(200);

      nock('http://localhost/')
        .get('/api/r/translate', {
          text: 'text',
        })
        .reply(200);

      store.dispatch(
        words.translateWord(0, 'text'))
        .then(() => done());
    });
  });

  describe('translateWordResponse()', () => {
    it('dispatches UPDATE_WORD', () => {
      store.dispatch(words.translateWordResponse(0, { text: ['translate'] }));

      const action = store.getActions()[0];
      expect(action.type).to.equal(UPDATE_WORD);
      expect(action.id).to.equal(0);
      expect(action.word.translate).to.equal('translate');
    });
  });
});
