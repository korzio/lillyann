import axios from 'axios';
import { ADD_WORD } from './constants';

export const receiveMyWords = words => dispatch => {
  words.forEach(word => {
    dispatch({
      type: ADD_WORD,
      word,
    });
  });
};

export const getMyWords = () => dispatch =>
  axios.get('/api/dictionary')
    .then(({ data: words }) =>
      dispatch(receiveMyWords(words))
    );
