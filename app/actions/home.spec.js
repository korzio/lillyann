import { expect } from 'chai';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';

import {
  UPDATE_WORD,
  REMOVE_WORD_HOME,
  SELECT_WORDS_HOME,
  ADD_WORD_HOME,
  ADD_WORD,
} from './constants';
import * as home from './home';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('home actions', () => {
  let store;
  beforeEach(() => {
    store = mockStore({});
    nock.cleanAll();
    store.clearActions();
  });

  describe('getRecentlyAdded()', (done) => {
    it('triggers get request to `recent` url', () => {
      nock('http://localhost/')
        .get('/api/recent')
        .reply(200);

      store.dispatch(home.getRecentlyAdded())
        .then(() => done());
    });
  });

  describe('isKnownWord()', () => {
    it('dispatches UPDATE_WORD, REMOVE_WORD_HOME, SELECT_WORDS_HOME with known value', () => {
      const expectedActions = [
        { type: UPDATE_WORD, id: 0, word: { isKnown: true } },
        { type: REMOVE_WORD_HOME, id: 0 },
        { type: SELECT_WORDS_HOME, selection: [] },
      ];
      store.dispatch(home.isKnownWord(0, true));

      const actions = store.getActions();
      expect(actions).to.deep.equal(expectedActions);
    });
  });

  describe('addNewWord()', () => {
    it('dispatches ADD_WORD_HOME', () => {
      store.dispatch(home.addNewWord('text'));

      const action = store.getActions()[0];
      expect(action.type).to.equal(ADD_WORD_HOME);
      expect(action.word.text).to.equal('text');
    });
  });

  describe('receiveRecentlyAdded()', () => {
    it('dispatches ADD_WORD_HOME, ADD_WORD for each word', () => {
      const items = [1];
      const expectedActions = [
        { type: ADD_WORD_HOME, word: 1 },
        { type: ADD_WORD, word: 1 },
      ];

      store.dispatch(home.receiveRecentlyAdded(items));

      const actions = store.getActions();
      expect(actions).to.deep.equal(expectedActions);
    });
  });

  describe('selectWordsHome()', () => {
    it('triggers SELECT_WORDS_HOME with array of selection indexes', () => {
      const selection = [1, 2, 3];
      store.dispatch(home.selectWordsHome(selection));

      const action = store.getActions()[0];
      expect(action.type).to.equal(SELECT_WORDS_HOME);
      expect(action.selection).to.equal(selection);
    });
  });
});
