import React from 'react';
import { render } from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { fromJS } from 'immutable';
import thunkMiddleware from 'redux-thunk';

import Router from '../components/Routes';
import appStore from '../reducers/';

const initialState = fromJS(window.__INITIAL_STATE__);

const store = createStore(appStore, initialState, applyMiddleware(
  thunkMiddleware
));

render(
  <Provider store={store}>
    <Router />
  </Provider>,
  document.getElementById('app')
);
