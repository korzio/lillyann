import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import Subheader from 'material-ui/Subheader';
import WordsTable from './WordsTable';
import LessonToolbar from './LessonToolbar';
import CircularProgress from 'material-ui/CircularProgress';

import LessonProgressChart from './LessonProgressChart';
import { getLesson } from '../actions';

class Lesson extends Component {
  componentDidMount() {
    const { dispatch, lessonId } = this.props;

    dispatch(getLesson(lessonId));
  }

  render() {
    const { lesson } = this.props;
    if (!lesson) {
      return (
        <CircularProgress />
      );
    }

    const { name, words } = lesson;
    return (
      <div>
        <LessonToolbar lesson={lesson.id} />
        <Subheader>{name}</Subheader>
        <LessonProgressChart />
        <WordsTable items={words} />
      </div>
    );
  }
}

Lesson.propTypes = {
  lessonId: PropTypes.string.isRequired,
  lesson: PropTypes.object,
  dispatch: PropTypes.func.isRequired,
};

function mapStateToProps(state, ownProps) {
  const { lessonId } = ownProps.params;
  const collection = state.getIn(['lessons', 'collection']).toJSON();
  const lesson = collection.find(item => `${item.id}` === lessonId);

  return {
    lessonId,
    lesson,
  };
}

export default connect(mapStateToProps)(Lesson);
