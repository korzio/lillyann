export default {
  isSelected: {
    'actions#/selection/some': true,
    'actions#/selection/none': false,
    'actions#/selection/all': true,
  },
  mapSelectionIds: {
    'actions#/selection/some': (list, overlist) => overlist.map(index => list[index] && list[index].id),
    'actions#/selection/none': () => [],
    'actions#/selection/all': list => list.map(item => item.id),
  },
};
