import React, { Component, PropTypes } from 'react';

class CountdownTimer extends Component {
  getInitialState() {
    return {
      secondsRemaining: 0,
    };
  }

  componentDidMount() {
    this.setState({ secondsRemaining: this.props.secondsRemaining });
    this.interval = setInterval(this.tick, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  tick() {
    this.setState({ secondsRemaining: this.state.secondsRemaining - 1 });
    if (this.state.secondsRemaining <= 0) {
      clearInterval(this.interval);
    }
  }

  render() {
    return (
      <div>Seconds Remaining: {this.state.secondsRemaining}</div>
    );
  }
}
