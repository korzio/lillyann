import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import Subheader from 'material-ui/Subheader';
import WordsTable from './WordsTable';
import { getMyWords } from '../actions';

class MyDictionary extends Component {
  componentDidMount() {
    const { dispatch } = this.props;

    dispatch(getMyWords());
  }

  render() {
    const { collection } = this.props;

    return (
      <div>
        <Subheader>My Dictionary</Subheader>
        <WordsTable items={collection} selection={[]} />
      </div>
    );
  }
}

MyDictionary.propTypes = {
  collection: PropTypes.array.isRequired,
  dispatch: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  const collection = state.getIn(['words', 'collection']).toJSON();

  return {
    collection,
  };
}

export default connect(mapStateToProps)(MyDictionary);
