import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

import { Link } from 'react-router';
import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar';
import RaisedButton from 'material-ui/RaisedButton';

const LessonToolbar = ({ lessonId }) => (
  <Toolbar>
    <ToolbarGroup>
      <RaisedButton>
        <Link to={`/lessons/${lessonId}/learn`} className="waves-effect waves-light btn">
          Start Learn
        </Link>
      </RaisedButton>
    </ToolbarGroup>
  </Toolbar>
);

LessonToolbar.propTypes = {
  lessonId: PropTypes.string.isRequired,
};

function mapStateToProps(state, ownProps) {
  const { lesson } = ownProps;

  return {
    lessonId: `${lesson}`,
  };
}

export default connect(
  mapStateToProps
)(LessonToolbar);
