import React from 'react';
import { connect } from 'react-redux';

import TextField from 'material-ui/TextField';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import { addNewWord } from '../actions';

const WordNew = ({ onClick }) => {
  let input;

  return (
    <div className="row">
      <div className="col s12 m6">
        <div className="card">
          <div className="card-content white-text">
            <span className="card-title">
              <TextField
                ref={node => {
                  input = node;
                }}
                name="name"
                underlineShow={false}
                hintText="New Word"
                onKeyDown={(event) => {
                  if (event.keyCode === 13) {
                    event.preventDefault();
                    onClick(input.getValue());
                    input.input.value = '';
                  }
                }}
              />
            </span>
            <FloatingActionButton
              mini={true}
              className="right"
              onClick={event => {
                event.preventDefault();
                onClick(input.getValue());
                input.input.value = '';
              }}
            >
              <ContentAdd />
            </FloatingActionButton>
          </div>
        </div>
      </div>
    </div>
  );
};

const mapDispatchToProps = dispatch => ({
  onClick: (text) => {
    dispatch(addNewWord(text));
  },
});

export default connect(null, mapDispatchToProps)(WordNew);
