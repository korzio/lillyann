import React from 'react';

import { LineChart } from 'react-d3';

const viewBoxObject = {
  x: 0,
  y: 0,
  width: 500,
  height: 300,
};

const lineData = [
  {
    name: 'mistakes',
    values: [
      { x: 0, y: 20 },
      { x: 1, y: 18 },
      { x: 2, y: 20 },
      { x: 3, y: 17 },
      { x: 4, y: 14 },
      { x: 5, y: 10 },
    ],
    strokeWidth: 3,
    strokeDashArray: '5,5',
  },
  {
    name: 'time',
    values: [
      { x: 0, y: 160 },
      { x: 1, y: 180 },
      { x: 2, y: 120 },
      { x: 3, y: 80 },
      { x: 4, y: 90 },
      { x: 5, y: 50 },
    ],
  },
];

export default () => (
  <LineChart
    legend
    data={lineData}
    width="100%"
    height={viewBoxObject.height}
    viewBoxObject={viewBoxObject}
    title="Lesson Progress"
    yAxisLabel="Mistakes"
    xAxisLabel="Attempts"
    gridHorizontal
  />
);
