import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import Subheader from 'material-ui/Subheader';
import WordNew from './WordNew';
import WordsTable from './WordsTable';
import HomeToolbar from './HomeToolbar';
import { getRecentlyAdded, selectWordsHome } from '../actions';
import djvu from './schema/djvu';

class Home extends Component {
  componentDidMount() {
    const { dispatch } = this.props;

    dispatch(getRecentlyAdded());
  }

  render() {
    const { collection, selection, selectedWordsIds } = this.props;

    return (
      <div>
        <HomeToolbar selection={selectedWordsIds} />
        <Subheader>Latest</Subheader>
        <WordsTable items={collection} selection={selection} onSelect={selectWordsHome} />

        <Subheader>Add new word</Subheader>
        <WordNew />
      </div>
    );
  }
}

Home.propTypes = {
  collection: PropTypes.array.isRequired,
  selection: PropTypes.array.isRequired,
  selectedWordsIds: PropTypes.array.isRequired,
  dispatch: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  const { collection, selection } = state.getIn(['words', 'home']).toJSON();
  const selectedWordsIds = djvu.find(djvu.actions.mapSelectionIds, selection)(collection, selection);

  return {
    collection,
    selection,
    selectedWordsIds,
  };
}

export default connect(mapStateToProps)(Home);
