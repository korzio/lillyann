import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

import { loginFlow } from '../actions';

const getButtonClassName = (logged) => {
  let className = 'g-signin2';
  if (logged) {
    className += ' hide';
  }
  return className;
};

const Auth = ({ logged, children, initLoginFlow }) => {
  if (!logged) {
    initLoginFlow();
  }

  return (
    <div>
      <div className={getButtonClassName(logged)} data-onsuccess="onSignIn"></div>
      <div>{logged && children}</div>
    </div>
  );
};

Auth.propTypes = {
  logged: PropTypes.bool.isRequired,
  children: PropTypes.array.isRequired,
  initLoginFlow: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  logged: state.getIn(['user', 'logged']),
});

const mapDispatchToProps = (dispatch) => ({
  initLoginFlow: () => dispatch(loginFlow()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Auth);
