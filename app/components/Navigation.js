import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import { Link } from 'react-router';
import Avatar from 'material-ui/Avatar';
import AppBar from 'material-ui/AppBar';
import Drawer from 'material-ui/Drawer';
import { List, ListItem } from 'material-ui/List';
import Divider from 'material-ui/Divider';

import ActionHome from 'material-ui/svg-icons/action/home';
import ActionBook from 'material-ui/svg-icons/action/book';
import ActionSettings from 'material-ui/svg-icons/action/settings';
import ImageCollections from 'material-ui/svg-icons/image/collections';

import { selectLocationState } from '../selectors/';

class Navigation extends Component {
  constructor(props) {
    super(props);

    this.state = { open: false };
  }

  handleToggle() {
    this.setState({ open: !this.state.open });
  }

  handleClose(flag, reason) {
    if (!flag && reason === 'clickaway') {
      this.setState({ open: false });
    }
  }

  render() {
    const { children, userImage, userName, title } = this.props;
    const { open } = this.state;

    return (
      <div>
        <AppBar
          title={title}
          className="blue-grey lighten-4"
          onLeftIconButtonTouchTap={() => this.handleToggle()}
        />

        <Drawer
          open={open}
          docked={false}
          onRequestChange={(flag, reason) => this.handleClose(flag, reason)}
        >
          <List>
            <ListItem leftIcon={<ActionSettings />}>
              <Link to="/profile">
                <Avatar src={userImage}>{userImage ? '' : userName}</Avatar>
              </Link>
            </ListItem>
            <Divider />
            <ListItem leftIcon={<ActionHome />}>
              <Link to="/home">Home</Link>
            </ListItem>
            <ListItem leftIcon={<ImageCollections />}>
              <Link to="/lessons">Lessons</Link>
            </ListItem>
            <ListItem leftIcon={<ActionBook />}>
              <Link to="/dictionary">My Dictionary</Link>
            </ListItem>
          </List>
        </Drawer>

        {children}
      </div>
    );
  }
}

Navigation.propTypes = {
  userName: PropTypes.string.isRequired,
  userImage: PropTypes.string,
  title: PropTypes.string.isRequired,
  children: PropTypes.array,
};

function getUserName(user) {
  return !user || !user.name ? '' : user.name.split(' ').map(name => name.slice(0, 1)).join('');
}

function mapStateToProps(state, ownProps) {
  const user = state.getIn(['user', 'profile']);
  const userName = getUserName(user);
  const userImage = user && user.image;

  const { children } = ownProps;
  const { location: { title } } = selectLocationState(state);

  return {
    userName,
    userImage,
    children,
    title,
  };
}

export default connect(mapStateToProps)(Navigation);
