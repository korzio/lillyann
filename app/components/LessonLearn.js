import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import {
  Step,
  Stepper,
  StepLabel,
} from 'material-ui/Stepper';
import CircularProgress from 'material-ui/CircularProgress';

import WordCard from './WordCard';
import LessonProgressChart from './LessonProgressChart';
import { getLesson } from '../actions';

class LessonLearn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      finished: false,
      stepIndex: 0,
    };
  }

  componentDidMount() {
    const { dispatch, lessonId } = this.props;

    dispatch(getLesson(lessonId));
  }

  nextWord() {
    const { stepIndex } = this.state;
    const { lesson: { words } } = this.props;

    this.setState({
      stepIndex: stepIndex + 1,
      finished: stepIndex >= words.length - 1,
    });
  }

  render() {
    const { lesson } = this.props;
    if (!lesson) {
      return (
        <CircularProgress />
      );
    }

    const { finished, stepIndex } = this.state;
    const { words } = lesson;

    return (
      <div>
        <Stepper activeStep={+finished}>
          <Step>
            <StepLabel>Word {finished ? words.length : stepIndex + 1} of {words.length}</StepLabel>
          </Step>
          <Step>
            <StepLabel>Finish!</StepLabel>
          </Step>
        </Stepper>
        {finished ? (
          <div>
            <LessonProgressChart />
            <h4>That was nice!</h4>
            <p>You've learned N new words, which were added to your progressive dictionary.</p>
            <a
              href="#"
              onClick={(event) => {
                event.preventDefault();
                this.setState({ stepIndex: 0, finished: false });
              }}
            >
              Once again!
            </a>
          </div>
        ) : (
          <WordCard word={words[stepIndex]} onSubmit={() => this.nextWord()} />
        )}
      </div>
    );
  }
}

LessonLearn.propTypes = {
  lessonId: PropTypes.string.isRequired,
  lesson: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
};

function mapStateToProps(state, ownProps) {
  const { lessonId } = ownProps.params;
  const collection = state.getIn(['lessons', 'collection']).toJSON();
  const lesson = collection.find(item => `${item.id}` === lessonId);

  return {
    lessonId,
    lesson,
  };
}

export default connect(mapStateToProps)(LessonLearn);
