import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import RaisedButton from 'material-ui/RaisedButton';
import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar';
import Snackbar from 'material-ui/Snackbar';
import { Link } from 'react-router';

import { isKnownWord } from '../actions';

class HomeToolbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };
  }

  handleTouchTap() {
    this.setState({
      open: true,
    });
  }

  handleRequestClose() {
    this.setState({
      open: false,
    });
  }

  render() {
    const { disabled, setKnownWord, selected } = this.props;

    return (
      <Toolbar>
        <ToolbarGroup>
          <RaisedButton>
            <Link to="/lessons/recently" className="waves-effect waves-light btn">
              {disabled ? 'Learn All' : 'Learn'}
            </Link>
          </RaisedButton>
          <RaisedButton
            label="I know it!"
            disabled={disabled}
            onClick={() => setKnownWord(selected)}
            onTouchTap={() => this.handleTouchTap()}
          />
          <RaisedButton label="Add to lesson" disabled={disabled} />

          <Snackbar
            open={this.state.open}
            message="...Will see..."
            autoHideDuration={3000}
            onRequestClose={() => this.handleRequestClose()}
          />
        </ToolbarGroup>
      </Toolbar>
    );
  }
}

HomeToolbar.propTypes = {
  disabled: PropTypes.bool.isRequired,
  selected: PropTypes.array,
  setKnownWord: PropTypes.func.isRequired,
};

const mapStateToProps = (state, ownProps) => ({
  disabled: !ownProps.selection.length,
  selected: ownProps.selection,
});

const mapDispatchToProps = (dispatch) => ({
  setKnownWord: (ids) => {
    ids.forEach(id => dispatch(isKnownWord(id, true)));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeToolbar);
