import React from 'react';
import Auth from './Auth';

import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';

// https://github.com/zilverline/react-tap-event-plugin
injectTapEventPlugin();

import Navigation from './Navigation';

const App = React.createClass({
  render() {
    return (
      <MuiThemeProvider muiTheme={getMuiTheme()}>
        <Auth>
          <Navigation />
          {this.props.children}
        </Auth>
      </MuiThemeProvider>
    );
  },
});

export default App;
