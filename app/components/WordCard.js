import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import {
  Card,
  CardActions,
  CardText,
  CardHeader,
} from 'material-ui/Card';

class WordCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      valid: null,
    };
  }

  validate(inputValue) {
    const { translate } = this.props;
    const valid = translate === inputValue;

    this.setState({
      valid,
    });

    return valid;
  }

  hideHint(time = 1000) {
    setTimeout(() => this.setState({
      valid: true,
    }), time);
  }

  render() {
    const { text, translate, onSubmit } = this.props;
    const { valid } = this.state;
    const hintText = valid === false && translate;
    let input;

    const hintProps = { hintText };
    if (hintText) {
      hintProps.hintStyle = { opacity: +!valid };
      this.hideHint();
    }

    return (
      <Card>
        <CardHeader
          title={text}
        />
        <CardText>
          <TextField
            autoFocus
            underlineShow={false}
            ref={node => {
              input = node;
            }}
            {...hintProps}
            onKeyDown={(event) => {
              if (event.keyCode === 13) {
                const noErrors = this.validate(input.input.value);

                input.input.value = '';
                if (noErrors) {
                  onSubmit();
                }
              } else {
                this.hideHint(0);
              }
            }}
          />
        </CardText>

        <CardActions>
          <RaisedButton
            label="Dont know"
            primary
            onTouchTap={() => this.validate()}
          />
        </CardActions>
      </Card>
    );
  }
}

WordCard.propTypes = {
  text: PropTypes.string.isRequired,
  translate: PropTypes.string.isRequired,
  onSubmit: PropTypes.func.isRequired,
};

function mapStateToProps(state, ownProps) {
  const { word: { text, translate }, onSubmit } = ownProps;

  return {
    text,
    translate,
    onSubmit,
  };
}

export default connect(mapStateToProps)(WordCard);
