import React, { PropTypes } from 'react';
import { Router, Route, IndexRoute } from 'react-router';
import history from 'react-router/lib/browserHistory';
import { connect } from 'react-redux';

import App from './App';
import Home from './Home';
import Profile from './Profile';
import Lessons from './Lessons';
import Lesson from './Lesson';
import LessonLearn from './LessonLearn';
import MyDictionary from './MyDictionary';

import { routeInfo } from '../actions';

const Routes = ({ createElement }) => (
  <Router history={history} createElement={createElement}>
    <Route path="/" component={App}>
      <IndexRoute component={Home} />
      <Route path="home" component={Home} />
      <Route path="profile" component={Profile} />
      <Route path="lessons" component={Lessons} />
      <Route path="lessons/:lessonId" component={Lesson} />
      <Route path="lessons/:lessonId/learn" component={LessonLearn} />
      <Route path="dictionary" component={MyDictionary} />
    </Route>
  </Router>
);

Routes.propTypes = {
  createElement: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => ({
  createElement(Component, props) {
    if (Component !== App) {
      dispatch(routeInfo({
        title: Component.displayName.replace(/Connect\(|\)/g, ''),
        path: props.route.path,
      }));
    }

    return <Component {...props} />;
  },
});

export default connect(null, mapDispatchToProps)(Routes);
