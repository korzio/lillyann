import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import Subheader from 'material-ui/Subheader';
import RaisedButton from 'material-ui/RaisedButton';

const styles = {
  button: {
    margin: 12,
  },
  exampleImageInput: {
    cursor: 'pointer',
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    width: '100%',
    opacity: 0,
  },
};

class Profile extends Component {
  render() {
    return (
      <div>
        <Subheader>Profile</Subheader>

        <RaisedButton
          label="Export My Data"
          labelPosition="before"
          style={styles.button}
        >
          <input type="file" style={styles.exampleImageInput} />
        </RaisedButton>

        <RaisedButton label="Import My Data" />
      </div>
    );
  }
}

Profile.propTypes = {
};

function mapStateToProps(state, ownProps) {
  return {
  };
}

export default connect(mapStateToProps)(Profile);
