import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import TextField from 'material-ui/TextField';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import { addNewLesson } from '../actions';

class LessonNew extends Component {
  onClick() {
    const { dispatch } = this.props;
    const lesson = {
      name: this.refs.name.getValue(),
      description: this.refs.description.getValue(),
      url: this.refs.url.getValue(),
    };

    dispatch(addNewLesson(lesson));
  }

  render() {
    return (
      <div className="row">
        <div className="col s12 m6">
          <div className="card blue-grey darken-1">
            <div className="card-content white-text">
              <span className="card-title">
                <TextField
                  name="name"
                  ref="name"
                  underlineShow={false}
                  hintText="Lesson name"
                />
              </span>
              <div>
                <TextField
                  name="description"
                  ref="description"
                  underlineShow={false}
                  multiLine
                  rows={2}
                  hintText="You can add a new lesson or search for existing one."
                />
                <TextField
                  name="url"
                  ref="url"
                  underlineShow={false}
                  hintText="Link to lesson's article"
                />
              </div>
            </div>
            <div className="card-action">
              <FloatingActionButton
                mini
                onClick={event => {
                  event.preventDefault();
                  this.onClick();
                }}
              >
                <ContentAdd />
              </FloatingActionButton>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

LessonNew.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

export default connect()(LessonNew);
