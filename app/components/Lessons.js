import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import Subheader from 'material-ui/Subheader';
import { GridList, GridTile } from 'material-ui/GridList';
import { Link } from 'react-router';

import LessonNew from './LessonNew';
import { getLessons } from '../actions';

class Lessons extends Component {
  componentDidMount() {
    const { dispatch } = this.props;

    dispatch(getLessons());
  }

  render() {
    const { collection } = this.props;

    return (
      <div>
        <Subheader>Lessons</Subheader>

        <GridList cellHeight={200} >
          {[...collection].map(lesson =>
            <Link to={`/lessons/${lesson.id}`}>
              <GridTile
                key={lesson.id}
                title={lesson.name}
                subtitle={lesson.description}
              >
                <img src={lesson.img} />
              </GridTile>
            </Link>
          )}
        </GridList>

        <Subheader>New</Subheader>
        <LessonNew />
      </div>
    );
  }
}

Lessons.propTypes = {
  collection: PropTypes.array.isRequired,
};

function mapStateToProps(state) {
  const collection = state.getIn(['lessons', 'collection']).toJSON();

  return {
    collection,
  };
}

export default connect(mapStateToProps)(Lessons);
