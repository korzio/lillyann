import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import TextField from 'material-ui/TextField';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';

import { updateWord, translateWord } from '../actions';

const WordsTable = ({ items, selection, onChange, onBlur, onSelect }) => (
  <Table
    selectable
    multiSelectable
    onRowSelection={onSelect}
  >
    <TableHeader displaySelectAll={!!selection} >
      <TableRow>
        <TableHeaderColumn>Word</TableHeaderColumn>
        <TableHeaderColumn>Transale</TableHeaderColumn>
      </TableRow>
    </TableHeader>
    <TableBody
      deselectOnClickaway={false}
      displayRowCheckbox={!!selection}
    >
      {[...items].map((word, index) =>
        <TableRow
          key={`${word.id}:${word.translate}`}
          selected={!!selection && !!~selection.indexOf(index)}
        >
          <TableRowColumn>
            <TextField
              name="word"
              onClick={event => event.stopPropagation()}
              onChange={event => onChange(word.id, event.target.value)}
              onBlur={event => onBlur(word.id, event.target.value)}
              underlineShow={false}
              autoFocus={word.autoFocus}
              defaultValue={word.text}
            />
          </TableRowColumn>
          <TableRowColumn>
            <TextField
              name="translate"
              onClick={event => event.stopPropagation()}
              underlineShow={false}
              defaultValue={word.translate}
            />
          </TableRowColumn>
        </TableRow>
      )}
    </TableBody>
  </Table>
);

WordsTable.propTypes = {
  items: PropTypes.array.isRequired,
  selection: PropTypes.array,
  onSelect: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  onBlur: PropTypes.func.isRequired,
};

function mapStateToProps(state, ownProps) {
  const { items, selection, onSelect } = ownProps;

  return {
    items,
    selection,
    onSelect,
  };
}

const mapDispatchToProps = (dispatch) => ({
  onChange: (id, text) => {
    dispatch(updateWord(id, { text }));
  },
  onBlur: (id, text) => {
    dispatch(translateWord(id, text));
  },
  dispatch,
});

function mergeProps(stateProps, dispatchProps) {
  return Object.assign({}, stateProps, dispatchProps, {
    onSelect: (selection) => {
      const { onSelect } = stateProps;
      const { dispatch } = dispatchProps;
      return dispatch(onSelect(selection));
    },
  });
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(WordsTable);
