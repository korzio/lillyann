import { createStore } from 'redux';

import appStore from '../../reducers/';
import render from './index.html.js';
import getInitialState from '../mongodb/init.js';

function handleRender(req, res, next) {
  if (~req.url.indexOf('/api')) {
    return next();
  }

  const store = createStore(appStore, getInitialState(req));
  const initialState = store.getState();

  return res.send(render(initialState));
}

export const configure = (app) => {
  app.get('*', handleRender);
};
