export default (initialState) => `
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>LilyAnn Main</title>
        <link href="/vendor/roboto.css" rel="stylesheet" type="text/css">
        <link href="/vendor/icons.css" rel="stylesheet">
        <link href="/vendor/materialize.min.css" rel="stylesheet">
    </head>
    <body>
        <div id="app"></div>
        <script>
            window.__INITIAL_STATE__ = ${JSON.stringify(initialState)}
        </script>
        <script src="/client.js"></script>
    </body>
    </html>
`;
