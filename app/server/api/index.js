const axios = require('axios');
const config = require('../../../config/translate.json');

function configure(app) {
  app.get('/api/r/translate', translate);

  app.all('/api/:url', (req, res) => {
    res.json(require(`./mock/${req.params.url}.json`));
  });

  app.all('/api/:url/:nested', (req, res) => {
    res.json(require(`./mock/${req.params.url}/${req.params.nested}.json`));
  });
}

function translate(req, res) {
  axios.get(`${config.url}?key=${config.key}&text=${req.query.text}&lang=${req.query.lang}`)
    .then(response => res.json(response.data));
}

module.exports = {
  configure,
};
