var passport = require('passport');
var config = require('../../../config/social.json');
var appConfig = require('../../../config/app.json');
var _ = require('lodash');
var users = require('./../mongodb/users');

function paths(app){
	_.each(config, function(socialSettings, socialName){
		var loginPath = '/auth/' + socialName,
			callbackPath = loginPath + '/callback',
			successPath = '/',
			failurePath = loginPath + '/failure';

		app.get(loginPath, passport.authenticate(socialName, {
			scope : config[socialName].scope
		}));

		app.get(callbackPath, passport.authenticate(socialName, {
			successRedirect: successPath,
			failureRedirect: failurePath
		}));

		var Strategy = require(socialSettings.strategy).Strategy;
		passport.use(new Strategy(
			_.extend({
				// TODO move to config, use environment variables
				callbackURL: appConfig.DOMAIN + ':' + appConfig.HTTP_PORT + callbackPath
			}, socialSettings.auth),
			function (request, accessToken, refreshToken, profile, done) {
				// TODO check profile for `provider` preference exist
				users.save(profile).then(function(){
					done(null, profile);
				});
			}));
	});
}

function configure(app){
	// http://passportjs.org/docs/configure
	passport.serializeUser(function(user, done) {
		done(null, user);
	});

	passport.deserializeUser(function(socialProfile, done) {
		users.find(socialProfile).then(function(user){
			done(null, user)
		});
	});

	app.use(passport.initialize());
  	app.use(passport.session());

	paths(app);
}

module.exports = {
	configure: configure,
	paths: paths
};