# Authorization

## Run App

```
cd ../
npm run start:auth
```

Go to [localhost:3001](http://localhost:3001)

## Paths

- [http://localhost:3001/auth/google](http://localhost:3001/auth/google)

## Remove my persmissions

Go to [my profile](https://security.google.com/settings/security/permissions?pli=1),
remove Just Bees permissions