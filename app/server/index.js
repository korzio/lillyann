var express = require('express');
var config = require('./../../config/app.json');
var session = require('express-session');

const app = express();

app.use(session(config.SESSION));
app.use(express.static('public'));

require('./auth').configure(app);
require('./api').configure(app);
require('./../../public/server.js').configure(app);

app.listen(config.HTTP_PORT, function () {
  console.log('http://localhost:' + config.HTTP_PORT + '/ - a test application for auth');
});
