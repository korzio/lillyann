import { fromJS } from 'immutable';
import structure from '../../reducers/index.json';

export default function getInitialState(req) {
  if (req.user) {
    structure.user = Object.assign({ logged: true }, req.user);
  }

  return fromJS(structure);
}
