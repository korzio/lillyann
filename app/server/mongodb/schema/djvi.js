const djvi = require('djvi');
const env = djvi();

env.addSchema('dictionary', require('./dictionary.json'));

export default env;
