var MongoClient = require('mongodb').MongoClient;
var config = require('./../../../config/mongodb.json');

module.exports = MongoClient.connect(config.url);