var connection = require('.');
var djvi = require('djvi');

var env = new djvi();
env.addSchema('social', require('./schema/social.json'));
env.addSchema('users', require('./schema/users.json'));

function save(socialProfile) {
	return connection.then(function (db) {
		var users = db.collection('users');

		return find(socialProfile).then(function (user) {
			// TODO update existing user profile social data
			if (!user) {
				user = env.instance('users');
				user.social.push(socialProfile);
				return users.save(user);
			}

			return user;
		});
	});
}

function find(socialProfile){
	return connection.then(function (db) {
		var users = db.collection('users');

		return users.findOne({
			social: {
				$elemMatch: {
					provider: socialProfile.provider,
					id: socialProfile.id
				}
			}
		});
	});
}

module.exports = {
	save: save,
	find: find
};