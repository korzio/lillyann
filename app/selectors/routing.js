export function selectLocationState(state) {
  return state.getIn(['routing']).toJSON();
}
