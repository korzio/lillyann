import { Map, fromJS } from 'immutable';
import { lessons as lessonsStructure } from './index.json';
import {
  ADD_LESSON,
  UPDATE_LESSON,
} from './../actions';

const initialState = fromJS(lessonsStructure);

export default function lessons(state = initialState, action) {
  switch (action.type) {
    case ADD_LESSON:
      return state.updateIn(
        ['collection'],
        list => list.push(Map(action.lesson))
      );
    case UPDATE_LESSON:
      return state.updateIn(
        ['collection'],
        list => list.update(
          list.findIndex(item => item.get('id') === action.id),
          item => item && item.merge(action.lesson)
        )
      );
    default:
      return state;
  }
}
