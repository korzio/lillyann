import { expect } from 'chai';
import { fromJS } from 'immutable';
import { words as wordsStructure } from './index.json';
import words from './words';
import { ADD_WORD, UPDATE_WORD } from './../actions';

describe('words reducers', () => {
  it('has an initial state', () => {
    const initial = fromJS(wordsStructure).toJSON();
    const action = {};
    const nextState = words(undefined, action);

    expect(nextState).to.equal(fromJS(initial));
  });

  describe('ADD_WORD', () => {
    const initial = fromJS(wordsStructure).toJSON();
    const action = { type: ADD_WORD, word: { id: 1, text: 'text' } };

    it('add word to collection', () => {
      const nextState = words(undefined, action);
      initial.collection.push(action.word);

      expect(nextState).to.equal(fromJS(initial));
    });
  });

  describe('UPDATE_WORD', () => {
    const initial = fromJS(wordsStructure).toJSON();

    it('updates word with translation and text', () => {
      initial.collection.push({
        id: 1,
      });
      const initialState = fromJS(initial);
      const action = { type: UPDATE_WORD, id: 1, word: { translate: 'translate', text: 'text' } };
      const nextState = words(initialState, action);

      Object.assign(initial.collection[0], action.word);
      expect(nextState).to.equal(fromJS(initial));
    });
  });
});
