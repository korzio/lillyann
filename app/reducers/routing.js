import { fromJS } from 'immutable';
import { LOCATION_CHANGE } from './../actions';
const initialState = fromJS(require('./index.json').routing);

export default function user(state = initialState, action) {
  switch (action.type) {
    case LOCATION_CHANGE:
      return state
        .set(
          'location',
          action.info
        );

    default:
      return state;
  }
}
