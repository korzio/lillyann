import { expect } from 'chai';
import { fromJS } from 'immutable';
import generate from './generate-words-table';
import { words as wordsStructure } from './index.json';
import {
  UPDATE_WORD,
  REMOVE_WORD_HOME,
  ADD_WORD_HOME,
  SELECT_WORDS_HOME,
} from './../actions';

describe('generate reducers', () => {
  it('is a structure generator', () => {
    expect(generate).to.be.a('function');
  });

  const homeStructure = wordsStructure.home;
  const home = generate('home');

  describe('home reducers', () => {
    it('has an initial state', () => {
      const initial = fromJS(homeStructure).toJSON();
      const action = {};
      const nextState = home(undefined, action);

      expect(nextState).to.equal(fromJS(initial));
    });

    describe('ADD_WORD_HOME', () => {
      const initial = fromJS(homeStructure).toJSON();
      const action = { type: ADD_WORD_HOME, word: { id: 1, text: 'text' } };

      it('add word to collection', () => {
        const nextState = home(undefined, action);
        initial.collection.push(action.word);

        expect(nextState).to.equal(fromJS(initial));
      });
    });

    describe('REMOVE_WORD_HOME', () => {
      const initial = fromJS(homeStructure).toJSON();
      const action = { type: REMOVE_WORD_HOME, word: { id: 1 } };

      initial.collection.push(action.word);

      it('remove word from collection', () => {
        const nextState = home(undefined, action);
        initial.collection.length = 0;

        expect(nextState).to.equal(fromJS(initial));
      });
    });

    describe('SELECT_WORDS_HOME', () => {
      const initial = fromJS(homeStructure).toJSON();
      const action = { type: SELECT_WORDS_HOME, selection: [1] };

      it('updates selection collection', () => {
        const nextState = home(undefined, action);
        initial.selection = action.selection;

        expect(nextState).to.equal(fromJS(initial));
      });
    });

    describe('UPDATE_WORD', () => {
      const initial = fromJS(wordsStructure).toJSON();

      it('updates word with translation and text', () => {
        initial.collection.push({
          id: 1,
        });
        const initialState = fromJS(initial);
        const action = { type: UPDATE_WORD, id: 1, word: { translate: 'translate', text: 'text' } };
        const nextState = home(initialState, action);

        Object.assign(initial.collection[0], action.word);
        expect(nextState).to.equal(fromJS(initial));
      });
    });
  });
});
