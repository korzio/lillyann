import { expect } from 'chai';
import { fromJS } from 'immutable';
import { lessons as lessonsStructure } from './index.json';
import lessons from './lessons';
import {
  ADD_LESSON,
  UPDATE_LESSON,
} from './../actions';

describe('lessons reducers', () => {
  it('has an initial state', () => {
    const initial = fromJS(lessonsStructure).toJSON();
    const action = {};
    const nextState = lessons(undefined, action);

    expect(nextState).to.equal(fromJS(initial));
  });

  describe('ADD_LESSON', () => {
    const initial = fromJS(lessonsStructure).toJSON();
    const action = { type: ADD_LESSON, lesson: { id: 1, text: 'text' } };

    it('add lesson to collection', () => {
      const nextState = lessons(undefined, action);
      initial.collection.push(action.lesson);

      expect(nextState).to.equal(fromJS(initial));
    });
  });

  describe('UPDATE_LESSON', () => {
    const initial = fromJS(lessonsStructure).toJSON();

    it('updates lesson with name', () => {
      initial.collection.push({
        id: 1,
      });
      const initialState = fromJS(initial);
      const action = { type: UPDATE_LESSON, id: 1, word: { name: 'name' } };
      const nextState = lessons(initialState, action);

      Object.assign(initial.collection[0], action.lesson);
      expect(nextState).to.equal(fromJS(initial));
    });
  });
});
