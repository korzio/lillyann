import generateWordsTable from './generate-words-table';

const generatedReducer = generateWordsTable('home');

export default function home(state, action) {
  const newState = generatedReducer(state, action);
  return newState;
}
