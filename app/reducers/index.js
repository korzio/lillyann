import { combineReducers } from 'redux-immutable';

import words from './words';
import lessons from './lessons';
import user from './user';
import routing from './routing';

export default combineReducers({
  words,
  lessons,
  user,
  routing,
});
