import { List, Map, fromJS } from 'immutable';

const initialState = fromJS(require('./index.json').words.home);
import {
  UPDATE_WORD,
} from './../actions';

export default function generateWordsTable(prefix) {
  const PREFIX = prefix.toUpperCase();

  return function home(state = initialState, action) {
    switch (action.type) {
      case `ADD_WORD_${PREFIX}`:
        return state.updateIn(
          ['collection'],
          list => list.push(Map(action.word))
        );
      case UPDATE_WORD:
        return state.updateIn(
          ['collection'],
          list => list.update(
            list.findIndex(item => item.get('id') === action.id),
            item => item && item.merge(action.word)
          )
        );
      case `REMOVE_WORD_${PREFIX}`:
        return state.updateIn(
          ['collection'],
          list => list.delete(
            list.findIndex(word => word.get('id') === action.id)
          )
        );
      case `SELECT_WORDS_${PREFIX}`:
        return state
          .set(
            'selection',
            List(action.selection)
          );
      default:
        return state;
    }
  };
}
