import { Map, fromJS } from 'immutable';
import { combineReducers } from 'redux-immutable';
import home from './home';
import { words } from './index.json';
import { ADD_WORD, UPDATE_WORD } from './../actions';

const initialState = fromJS(words.collection);

function collection(state = initialState, action) {
  switch (action.type) {
    case ADD_WORD:
      return state.push(
        Map(action.word)
      );
    case UPDATE_WORD:
      return state.update(
        state.findIndex(item => item.get('id') === action.id),
        item => item.merge(action.word)
      );
    default:
      return state;
  }
}

export default combineReducers({
  collection,
  home,
});
