import { expect } from 'chai';
import { fromJS } from 'immutable';
import indexStructure from './index.json';
import index from './index';

describe('index reducers', () => {
  it('has an initial state', () => {
    const initial = fromJS(indexStructure).toJSON();
    const action = {};
    const nextState = index(undefined, action);

    expect(nextState).to.equal(fromJS(initial));
  });
});
