import { expect } from 'chai';
import { fromJS } from 'immutable';
import user from './user';
import { LOGIN } from './../actions';
import { user as userStructure } from './index.json';

describe('user reducers', () => {
  it('has an initial state', () => {
    const initial = fromJS(userStructure).toJSON();
    const action = {};
    const nextState = user(undefined, action);

    expect(nextState).to.equal(fromJS(initial));
  });

  describe('LOGIN', () => {
    const initial = fromJS(userStructure).toJSON();
    const action = { type: LOGIN };

    it('updates selection collection', () => {
      const nextState = user(undefined, action);
      initial.logged = true;

      expect(nextState).to.equal(fromJS(initial));
    });
  });
});
