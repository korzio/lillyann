import { fromJS } from 'immutable';
import { LOGIN } from './../actions';
const initialState = fromJS(require('./index.json').user);

export default function user(state = initialState, action) {
  switch (action.type) {
    case LOGIN:
      return state
        .set('logged', true)
        .set('profile', action.profile);

    default:
      return state;
  }
}
