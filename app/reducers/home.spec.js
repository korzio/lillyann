import { expect } from 'chai';
import { fromJS } from 'immutable';
import home from './home';
import { words as wordsStructure } from './index.json';
const homeStructure = wordsStructure.home;
import {
  SELECT_WORDS_HOME,
} from './../actions';

describe('home reducers', () => {
  it('has an initial state', () => {
    const initial = fromJS(homeStructure).toJSON();
    const action = {};
    const nextState = home(undefined, action);

    expect(nextState).to.equal(fromJS(initial));
  });

  describe('SELECT_WORDS_HOME', () => {
    const initial = fromJS(homeStructure).toJSON();
    const action = { type: SELECT_WORDS_HOME, selection: [1] };

    it('updates selection', () => {
      const nextState = home(undefined, action);
      initial.selection = action.selection;

      expect(nextState).to.equal(fromJS(initial));
    });
  });
});
